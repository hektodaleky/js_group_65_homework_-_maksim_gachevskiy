import axios from "axios";

const instance = axios.create({
    baseURL: "https://mgachevskiy-react-hw.firebaseio.com/"
});
export default instance;