import React, {Component} from "react";
import constans from "../../constans";
import axios from "../../axios-orders";
import './EditInfo.css'

class EditInfo extends Component {
    state = {
        category: "",
        title: "",
        text: "",
        loading: false,
        info: []
    };

    getInfo = () => {

        axios.get(`/${this.state.category}.json`).then(response => {

            let info;
            for (let key in response.data) {
                console.log(response.data, key)

                info = {...response.data[key], key}


            }
            if (info)
                this.setState({info: info, loading: false, category: this.props.match.params.category});
            console.log(info)

        }).catch(response => {
            this.setState({loading: false});
        })
    };
    changeStatus = event => {
        this.setState({category: event.target.value})
    };

    changeText = (event, id) => {
        this.setState({[id]: event.target.value});
    };


    sendChanges = event => {
        event.preventDefault();
        if (!(this.state.category && this.state.title && this.state.text)) {
            alert("Заполните все поля");
            return;
        }
        let info;

        axios.get(`/${this.state.category}.json`).then(response => {


            for (let key in response.data) {

                info = key;


            }

            axios.put(`/${this.state.category}/${info}.json`, {title: this.state.title, text: this.state.text}
            ).then(() => {
                this.props.history.push({
                    pathname: '/'
                });
            });
        })


    };


    render() {
        return (<form className="form-editor">
            <select onChange={this.changeStatus} value={this.state.category}>
                <option value=""></option>
                {constans.category.map((item, index) => <option key={item.id + "" + index}
                                                                value={item.id}>{item.text}</option>)}
            </select>
            <label htmlFor="title">Title</label>
            <input id="title" onChange={(event => {
                this.changeText(event, event.target.id)
            })} value={this.state.title}/>
            <label htmlFor="text">MainText</label>
            <textarea id="text"
                      onChange={(event => {
                          this.changeText(event, event.target.id)
                      })} value={this.state.text}></textarea>
            <button onClick={this.sendChanges}>Send Changes</button>
        </form>)
    }

}
;
export default EditInfo;