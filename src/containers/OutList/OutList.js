import React, {Component} from "react";
import Item from "../../components/Item/Item";
import axios from "../../axios-orders";
import Spinner from "../../components/UI/Spinner/Spinner";
class OutList extends Component {
    state = {
        info: [],
        loading: true,
        category: null
    };
    getInfo = () => {

            axios.get(`/${this.props.match.params.category}.json`).then(response => {

                let info;
                for (let key in response.data) {

                    info = response.data[key]


                }
                if (info)
                    this.setState({info: info, loading: false, category: this.props.match.params.category});
                console.log(info)

            }).catch(response => {
                this.setState({loading: false});
            })
    };


    componentDidMount() {
        this.getInfo();


    };

    componentDidUpdate() {

        if (this.props.match.params.category != this.state.category) {
            this.setState({loading: true, category: this.props.match.params.category});
            this.getInfo();
        }
    }

    render() {
        if (this.state.loading)
            return (<Spinner/>);
        else

            return (<Item title={this.state.info.title}
                          text={this.state.info.text}/>)
    }
}
;
export default OutList;