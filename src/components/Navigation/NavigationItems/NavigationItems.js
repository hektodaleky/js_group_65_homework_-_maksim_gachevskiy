import React from "react";
import "./NavigationItems.css";
import NavigationItem from "./NavigationItem/NavigationItem";
import constans from "../../../constans";

const NavigationItems = () => (
    <ul className="NavigationItems">

        {constans.category.map((item,index) => <NavigationItem key={item.id+""+index} to={`/pages/${item.id}`}>{item.text}</NavigationItem>)}
        <NavigationItem to="/admin/" exact>Admin</NavigationItem>
    </ul>
);
export default NavigationItems;