import React from "react";
import './Item.css';
const Item = props => {
    return (
        <div className="Item">
            <h1>{props.title}</h1>
            <p>{props.text}</p>
        </div>
    )
};
export default Item;