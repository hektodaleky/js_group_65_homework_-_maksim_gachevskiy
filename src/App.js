import React, { Component } from 'react';
import './App.css';
import {Route, Switch} from "react-router-dom";
import Layout from "./components/Layout/Layout";
import OutList from "./containers/OutList/OutList";
import EditInfo from "./containers/EditInfo/EditInfo";

class App extends Component {
    render() {
        return (
            <Layout>
                <Switch>
                    <Route path='/' exact render={() => <h1>Select Category</h1>}/>
                    <Route path="/admin" component={EditInfo}/>
                    <Route path="/pages/:category" component={OutList}/>
                    <Route render={() => <h1>Not found</h1>}/>
                </Switch>
            </Layout>
        );
    }
}
export default App;
